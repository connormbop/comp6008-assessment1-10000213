﻿using System;
using System.Diagnostics;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;
using Rand;

namespace RandNumTask1
{
    public partial class MainPage : ContentPage
    {
        public MainPage()
        {
            InitializeComponent();
        }

        public void submitBtn_Click(object Sender, EventArgs e)
        {

            var one = int.Parse(entryBox.Text);

            var a = new Randy(one);

            var random = a.numGen();
            var colour = a.Col(random);
            var wL = a.Winner(random);

            background.BackgroundColor = Color.FromHex($"{colour}");
            compAns.Text = ($"Wins:{wL} Random Number:{random}");
        }
    }

}
