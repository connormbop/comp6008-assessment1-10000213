﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Rand
{
    public class Randy
    {
        public int numUI;
        public static int wins;

        public Randy(int _one)
        {
            numUI = _one;
        }
        public int Winner(int random)
        {
            int num;
            num = numUI;
            if (num == random)
            {
                wins++;
            }
            return wins;
        }

        public string Col(int random)
        {
            int num;
            string c1;
            num = numUI;

            if (num == random)
            {
                c1 = "#00FF00";
            }
            else
            {
                c1 = "#FF0000";
            }
            return c1;
        }
        public int numGen()
        {
            Random rnd = new Random();
            var randNum = rnd.Next(1, 5);
            return randNum;
        }
    }
}
