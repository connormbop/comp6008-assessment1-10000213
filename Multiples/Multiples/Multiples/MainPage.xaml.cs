﻿using System;
using System.Diagnostics;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;
using Multiples;

namespace Multiples
{    
    public partial class MainPage : ContentPage
    {
        public MainPage()
        {
            InitializeComponent();
        }
        public void Button_Clicked(object Sender, EventArgs e)
        {
            var one = int.Parse(userinput.Text);

            var a = new Multiple(one);

            var answer = a.userInput(one.ToString());

            output.Text = answer.ToString();
        }
     }

}
