﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Multiples
{
    class Multiple
    {
        public int uI;
        public string input;
        public int m3;
        public int m5;
        public int o3;
        public int o5;
        public int outputs;

        public Multiple(int _one)
        {
            uI = _one;
        }

        public int userInput(string _input)
        {
            for (m3 = 0; m3 < uI; m3++)
            {
                if (m3 % 3 == 0)
                {
                    o3 += m3;
                }
            }
            for (m5 = 0; m5 < uI; m5++)
            {
                if (m5 % 5 == 0)
                {
                    o5 += m5;
                }
            }
            outputs = o3 + o5;
            return outputs;
        }

    }
}
